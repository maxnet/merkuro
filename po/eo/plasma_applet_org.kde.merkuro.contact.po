# translation of plasma_applet_org.kde.merkuro.contact.pot to esperanto
# Copyright (C) 2023 Free Software Foundation, Inc.
# This file is distributed under the same license as the merkuro package.
# Oliver Kellogg <okellogg@users.sourceforge.net, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: merkuro\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-11 01:35+0000\n"
"PO-Revision-Date: 2023-09-28 21:14+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/ui/ContactPage.qml:49
#, kde-format
msgid "Return to Contact List"
msgstr "Reveni al Kontaktlisto"

#: package/contents/ui/ContactPage.qml:56
#: package/contents/ui/ContactPage.qml:57
#: package/contents/ui/ContactPage.qml:243
#, kde-format
msgid "Call"
msgstr "Voki"

#: package/contents/ui/ContactPage.qml:69
#: package/contents/ui/ContactPage.qml:101
#, kde-format
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: package/contents/ui/ContactPage.qml:88
#: package/contents/ui/ContactPage.qml:89
#, kde-format
msgid "Send SMS"
msgstr "Sendi SMS"

#: package/contents/ui/ContactPage.qml:119
#: package/contents/ui/ContactPage.qml:120
#: package/contents/ui/ContactPage.qml:214
#, kde-format
msgid "Send Email"
msgstr "Sendi Retpoŝton"

#: package/contents/ui/ContactPage.qml:128
#: package/contents/ui/ContactPage.qml:129
#, kde-format
msgid "Show QR Code"
msgstr "Montri QR-Kodon"

#: package/contents/ui/ContactPage.qml:159
#, kde-format
msgid "Nickname: %1"
msgstr "Kromnomo: %1"

#: package/contents/ui/ContactPage.qml:169
#: package/contents/ui/ContactPage.qml:173
#, kde-format
msgid "Birthday:"
msgstr "Naskiĝtago"

#: package/contents/ui/ContactPage.qml:171
#, kde-format
msgctxt "Day month format"
msgid "dd.MM."
msgstr "dd.MM."

#: package/contents/ui/ContactPage.qml:182
#, kde-format
msgid "Address"
msgid_plural "Addresses"
msgstr[0] "Adreso"
msgstr[1] "Adresoj"

#: package/contents/ui/ContactPage.qml:193
#, kde-format
msgctxt "%1 is the type of the address, e.g. home, work, ..."
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/ContactPage.qml:202
#, kde-format
msgid "Email Address"
msgid_plural "Email Addresses"
msgstr[0] "Retpoŝta Adreso"
msgstr[1] "Retpoŝtaj Adresoj"

#: package/contents/ui/ContactPage.qml:231
#, kde-format
msgid "Phone number"
msgid_plural "Phone numbers"
msgstr[0] "Telefonnumero"
msgstr[1] "Telefonnumeroj"

#: package/contents/ui/ContactPage.qml:240
#, kde-format
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/ContactsPage.qml:18
#, kde-format
msgid "Contacts"
msgstr "Kontaktoj"

#: package/contents/ui/main.qml:17 package/contents/ui/main.qml:33
#, kde-format
msgid "Contact"
msgstr "Kontakto"

#: package/contents/ui/QrCodePage.qml:21 package/contents/ui/QrCodePage.qml:52
#, kde-format
msgid "QR Code"
msgstr "QR-Kodo"

#: package/contents/ui/QrCodePage.qml:35
#, kde-format
msgid "Return to Contact"
msgstr "Reveni al Kontakto"

#: package/contents/ui/QrCodePage.qml:53
#, kde-format
msgid "Data Matrix"
msgstr "Data Matrix"

#: package/contents/ui/QrCodePage.qml:54
#, kde-format
msgctxt "Aztec barcode"
msgid "Aztec"
msgstr "Aztec"

#: package/contents/ui/QrCodePage.qml:78
#, kde-format
msgid "Change the QR code type"
msgstr "Ŝanĝi la QR-kodan tipon"

#: package/contents/ui/QrCodePage.qml:102
#, kde-format
msgid "Creating QR code failed"
msgstr "Kreado de QR-kodo malsukcesis"

#: package/contents/ui/QrCodePage.qml:111
#, kde-format
msgid "The QR code is too large to be displayed"
msgstr "La QR-kodo estas tro granda por bildigo"
